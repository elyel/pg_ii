
package br.com.elyel.pgii.trabalho01.visualizacao;


import br.com.elyel.pgii.trabalho01.classes.Objeto2D;
import br.com.elyel.pgii.trabalho01.classes.Ponto;
import br.com.elyel.pgii.trabalho01.classes.ViewPort;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.swing.JPanel;
import javax.swing.text.Position;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class ViewPortFrame extends JPanel{
    private int yTotal = this.getSize().height;
    private int xTotal = this.getSize().width;
    private int yCentro = (int) (yTotal * 0.5);
    private int xCentro = (int) (xTotal * 0.5);
    
    private final ViewPort viewPort;
    
    private boolean movimentar;
    private Point inicioMovimento;
    private Point ultimoMovimento;
    private boolean movimentando = false;
    
    private final Cursor moveCursor = new Cursor(Cursor.MOVE_CURSOR);
    private final Cursor defaultCursur = new Cursor(Cursor.DEFAULT_CURSOR);
    
    public ViewPortFrame(final ViewPort viewPort) {
        super();
        this.viewPort = viewPort;
        adicionarMouseWheelListener();
        adicionarMouseMotionListener();
        adicionarMouseListener();
        adicionarKeyPressedEvent();
    }
    
    
    public void desenhar(){
        repaint();
    }
    
    
    @Override
    protected void processMouseMotionEvent(MouseEvent e) {
        super.processMouseMotionEvent(e);
    }
    
    

    @Override
    protected void processMouseEvent(MouseEvent e) {
        super.processMouseEvent(e);
    }
    
    

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        
        atualizarCoordenadasMaxMin();
        desenharCentroDoPlanoCartesiano(g);
        desenharObjetos(g);
    }

    @Override
    public void paintComponents(Graphics g) {
        super.paintComponents(g);
    }
    
    

    /**
     * Desenha duas linhas em Diagonal e Horizontal, para marcar o centro do
     * plano com a cor cinza claro.
     * 
     * @param g 
     */
    private void desenharCentroDoPlanoCartesiano(Graphics g) {
        g.setColor(Color.lightGray);
        desenharLinha(viewPort.getWindow().getLinhaX(), g);
        desenharLinha(viewPort.getWindow().getLinhaY(), g);
    }

    private void atualizarCoordenadasMaxMin() {
        this.yTotal = this.getSize().height;
        this.xTotal = this.getSize().width;
        this.yCentro = (int) (yTotal * 0.5);
        this.xCentro = (int) (xTotal * 0.5);
        
        if (this.getSize().width < this.getSize().height) {
            viewPort.setMaxX(this.getSize().width);
            viewPort.setMaxY(this.getSize().width);
        } else {
            int metadeDaSobraXparaY =  (int) (this.getSize().width-this.getSize().height) / 2;
            viewPort.setMaxX(this.getSize().width - metadeDaSobraXparaY);
            viewPort.setMinX(metadeDaSobraXparaY);
            viewPort.setMaxY(this.getSize().height);
            viewPort.setMinY(0);
        }
        
        
        
        
        System.out.println("(X:" + xTotal + ", Y:" + yTotal + ")");
    }

    private void desenharObjetos(Graphics g) {
        for(Objeto2D objeto2D : this.viewPort.getWindow().getObjetos()){
            if(null != objeto2D.getTipo())switch (objeto2D.getTipo()) {
                case PONTO:
                    desenharPonto(objeto2D, g);
                    break;
                case LINHA:
                    desenharLinha(objeto2D, g);
                    break;
                case POLIGONO:
                    desenharPoligono(objeto2D, g);
                    break;
                default:
                    break;
            }
        }
    }
    
    private void desenharPonto(Objeto2D objeto2D, Graphics g){
        printInfo("DESENHAR PONTO: " + objeto2D.getNome()); 
        
        if(objeto2D.getCoordenadas() == null){
            printErro("ERRO AO DESENHAR PONTO: " + objeto2D.getNome() 
                    + " não possui coordenadas"); 
        }
        
        if(objeto2D.getCoordenadas().isEmpty()){
            printErro("ERRO AO DESENHAR PONTO: " + objeto2D.getNome() 
                    + " não possui coordenadas"); 
        }
        
        Ponto pontoWindow = objeto2D.getCoordenadas().get(0);
        Ponto pontoViewPort = windowParaViewPort(pontoWindow);
        
        
        int x = pontoViewPort.getX().intValue();
        int y = pontoViewPort.getY().intValue();
        
        g.setColor(Color.blue);
        g.drawLine(x, y, x, y);
        g.drawLine((x-1), y, (x+1), y);
        g.drawLine(x, (y-1), x, (y+1));
        
        
    }

    private void desenharLinha(Objeto2D objeto2D, Graphics g) {
        if(objeto2D == null) return;
        if(g == null) return;
        
        printInfo("DESENHAR LINHA/RETA: " + objeto2D.getNome()); 
        
        if(objeto2D.getCoordenadas() == null){
            printErro("ERRO AO DESENHAR LINHA: " + objeto2D.getNome() 
                    + " não possui coordenadas"); 
        }
        
        if(objeto2D.getCoordenadas().isEmpty()){
            printErro("ERRO AO DESENHAR LINHA: " + objeto2D.getNome() 
                    + " não possui coordenadas"); 
        }
        
        
        Ponto pontoAnterior = null;
        for(Ponto pontoWindow : objeto2D.getCoordenadas()){
            Ponto pontoViewPort = windowParaViewPort(pontoWindow);
            
            if(pontoAnterior == null){
                pontoAnterior = pontoViewPort;
                continue;
            }
            
            printInfo("LINHA [" 
                    + pontoAnterior.getX().intValue() + ", "
                    + pontoAnterior.getY().intValue() + ", "
                    + pontoViewPort.getX().intValue() + ", " 
                    + pontoViewPort.getY().intValue()
                    + "]"
            );
            
            g.drawLine(
                    pontoAnterior.getX().intValue(), 
                    pontoAnterior.getY().intValue(), 
                    pontoViewPort.getX().intValue(), 
                    pontoViewPort.getY().intValue()
            );
            
            pontoAnterior = pontoViewPort;
        }
    }

    private void desenharPoligono(Objeto2D objeto2D, Graphics g) {
        if(objeto2D.getCoordenadas() == null){
            printErro("ERRO AO DESENHAR LINHA: " + objeto2D.getNome() 
                    + " não possui coordenadas"); 
        }
        
        if(objeto2D.getCoordenadas().isEmpty()){
            printErro("ERRO AO DESENHAR LINHA: " + objeto2D.getNome() 
                    + " não possui coordenadas"); 
        }
        
        g.setColor(Color.blue);
        
        Ponto pontoAnterior = null;
        Ponto primeiroPonto = null;
        for(Ponto pontoWindow : objeto2D.getCoordenadas()){
            Ponto pontoViewPort = windowParaViewPort(pontoWindow);
            
            if(primeiroPonto == null){
                primeiroPonto = pontoViewPort;
            }
            
            if(pontoAnterior == null){
                pontoAnterior = pontoViewPort;
                continue;
            }
            
            printInfo("LINHA [" 
                    + pontoAnterior.getX().intValue() + ", "
                    + pontoAnterior.getY().intValue() + ", "
                    + pontoViewPort.getX().intValue() + ", " 
                    + pontoViewPort.getY().intValue()
                    + "]"
            );
            
            g.drawLine(
                    pontoAnterior.getX().intValue(), 
                    pontoAnterior.getY().intValue(), 
                    pontoViewPort.getX().intValue(), 
                    pontoViewPort.getY().intValue()
            );
            
            pontoAnterior = pontoViewPort;
        }
        
//        if (pontoAnterior != null & primeiroPonto != null) {
//            g.drawLine(
//                    pontoAnterior.getX().intValue(),
//                    pontoAnterior.getY().intValue(),
//                    primeiroPonto.getX().intValue(),
//                    primeiroPonto.getY().intValue()
//            );
//        }
    }

    private void printInfo(String string) {
        System.out.println("INFO: " + string);
    }

    private void printErro(String string) {
        System.out.println("ERRO: " + string);
    }

    private Ponto windowParaViewPort(Ponto pontoWindow) {
        
        BigDecimal xViewPortMin = new BigDecimal(this.viewPort.getMinX());
        BigDecimal xViewPortMax = new BigDecimal(this.viewPort.getMaxX());
        BigDecimal yViewPortMin = new BigDecimal(this.viewPort.getMinY());
        BigDecimal yViewPortMax = new BigDecimal(this.viewPort.getMaxY());
        
        BigDecimal xWindowMin = this.viewPort.getWindow().getMinX();
        BigDecimal xWindowMax = this.viewPort.getWindow().getMaxX();
        BigDecimal yWindowMin = this.viewPort.getWindow().getMinY();
        BigDecimal yWindowMax = this.viewPort.getWindow().getMaxY();
        
        BigDecimal xWindow = pontoWindow.getX();
        BigDecimal yWindow = pontoWindow.getY();
        
        BigDecimal xViewPort = 
                xViewPortMin
                    .add(xWindow.subtract(xWindowMin)
                            .divide(xWindowMax.subtract(xWindowMin), 3, RoundingMode.HALF_UP)
                            .multiply(xViewPortMax.subtract(xViewPortMin))
                    );
        
        BigDecimal yViewPort = 
                yViewPortMax
                    .subtract(yWindow.subtract(yWindowMin)
                            .divide(yWindowMax.subtract(yWindowMin), 3, RoundingMode.HALF_UP)
                            .multiply(yViewPortMax.subtract(yViewPortMin))
                    );
        
        Ponto pontoViewPort = new Ponto(xViewPort, yViewPort, BigDecimal.ZERO);
    
        printInfo("W para VP: [" + pontoWindow.getX().intValue() + ", " + pontoWindow.getY().intValue() + ", " + pontoWindow.getZ().intValue() + "] = ["
                                + pontoViewPort.getX().intValue() + ", " + pontoViewPort.getY().intValue() + ", " + pontoViewPort.getZ().intValue() + "]"
        );
        
        return pontoViewPort;
    }
    
    

    public boolean isMovimentar() {
        return movimentar;
    }

    public void setMovimentar(boolean movimentar) {
        this.movimentar = movimentar;
    }
    
    private void atualizarMovimento(){
        if(ultimoMovimento == null) return;
        if(inicioMovimento == null) return;
        
        if(movimentar){
            double movimentoX = -1 * (ultimoMovimento.getX() - inicioMovimento.getX());
            double movimentoY = ultimoMovimento.getY() - inicioMovimento.getY();
            
            this.viewPort.getWindow().getDeslocamento().addX(new BigDecimal(movimentoX));
            this.viewPort.getWindow().getDeslocamento().addY(new BigDecimal(movimentoY));
        }
    }

    private void adicionarMouseWheelListener() {
        this.addMouseWheelListener(
                new MouseWheelListener() {
                    @Override
                    public void mouseWheelMoved(MouseWheelEvent e) {
                        if (e.getWheelRotation() < 0) {
                            viewPort.getWindow().setZoom(viewPort.getWindow().getZoom().add(new BigDecimal(0.01)));
                            desenhar();
                        }

                        if (e.getWheelRotation() > 0) {
                            viewPort.getWindow().setZoom(viewPort.getWindow().getZoom().subtract(new BigDecimal(0.01)));
                            desenhar();
                        }
                    }
                }
        );
    }

    private void adicionarMouseMotionListener() {
        this.addMouseMotionListener(
                new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                printInfo("Mouse Dragged: Point [" + e.getPoint().getX() + ", " 
                + e.getPoint().getY() + "], Posiction on screen [" 
                + e.getLocationOnScreen().getX() + ", " + e.getLocationOnScreen().getY() + "]");
             
                if(movimentar && movimentando){
                    inicioMovimento = ultimoMovimento;
                    ultimoMovimento = e.getPoint();
                    atualizarMovimento();
                    desenhar();
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                
            }
        }
        );
    }

    private void adicionarMouseListener() {
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if(movimentar){
                    printInfo("Inicio Movimento.");
                    inicioMovimento = e.getPoint();
                    movimentando = true;
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(movimentar){
                    printInfo("Fim Movimento.");
                    ultimoMovimento = e.getPoint();
                    movimentando = false;
                    ultimoMovimento = null;
                    inicioMovimento = null;
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if(movimentar){
                    setCursor(moveCursor);
                } else {
                    setCursor(defaultCursur);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setCursor(defaultCursur);
            }
        });
    }

    private void adicionarKeyPressedEvent() {
        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(movimentar){
                    switch(e.getKeyCode()){
                        case KeyEvent.VK_UP:
                            viewPort.getWindow().getDeslocamento().addX(BigDecimal.ONE);
                            break;
                        case KeyEvent.VK_DOWN:
                            viewPort.getWindow().getDeslocamento().subX(BigDecimal.ONE);
                            break;
                        case KeyEvent.VK_LEFT:
                            viewPort.getWindow().getDeslocamento().subY(BigDecimal.ONE);
                            break;
                        case KeyEvent.VK_RIGHT:
                            viewPort.getWindow().getDeslocamento().addY(BigDecimal.ONE);
                            break;
                    }
                    desenhar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
    }
    
    
}
