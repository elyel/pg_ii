package br.com.elyel.pgii.trabalho01.visualizacao;

import br.com.elyel.pgii.trabalho01.classes.Ponto;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class PontoTableModel  extends AbstractTableModel{
    private static final long serialVersionUID = 210721851360409637L;
    
    /* Lista de Pontos que representam as linhas */
    private List<Ponto> linhas;
    
    /* indice de colunas */
    public static final int COL_SEQ = 0;
    public static final int COL_X = 1;
    public static final int COL_Y = 2;
    public static final int COL_Z = 3;
    
    /* Array de Strings com o nome das colunas. */ 
    private final String[] colunas = new String[]{
        "Sequencia", "X", "Y", "Z"
    };
    
    /* cria uma PontosTableModel Vazia */
    public PontoTableModel() {
    }
    
    /* cria uma PontosTableModel com dados */
    public PontoTableModel(List<Ponto> linhas) {
        this.linhas = linhas;
    }
    
    /* Retorna a quantidade de colunas */
    @Override
    public int getRowCount() {
        /* retorna o tamanho do array de linhas */
        try{
            return this.linhas.size();
        }catch(Exception e){
            return 0;
        }
    }

    /* Retorna a quantidade de linhas. */ 
    @Override
    public int getColumnCount() {
        // Retorna o tamanho da lista de dados
        return this.colunas.length;
    }

    /* Retorna o nome da coluna no índice especificado.   
     * Este método é usado pela JTable para saber o texto do cabeçalho. */      
    @Override      
    public String getColumnName(int columnIndex) {      
        // Retorna o conteúdo do Array que possui o nome das colunas      
        // no índice especificado.      
        return colunas[columnIndex];      
    };      
    
    
      
    /* Retorna a classe dos elementos da coluna especificada.   
     * Este método é usado pela JTable na hora de definir o editor da célula. */      
    @Override      
    public Class<?> getColumnClass(int columnIndex) {      
        // Retorna a classe referente a coluna especificada.      
        // Aqui é feito um switch para verificar qual é a coluna      
        // e retornar o tipo adequado. As colunas são as mesmas      
        // que foram especificadas no array "colunas".      
        switch (columnIndex) {      
        case COL_SEQ:
            return Integer.class;      
        case COL_X:
            return String.class;  
        case COL_Y:
            return String.class;   
        case COL_Z:
            return String.class;   
        default:      
            // Se o índice da coluna não for válido, lança um      
            // IndexOutOfBoundsException (Exceção de índice fora dos limites).      
            // Não foi necessário verificar se o índice da linha é inválido,      
            // pois o próprio ArrayList lança a exceção caso seja inválido.      
            throw new IndexOutOfBoundsException("columnIndex out of bounds");      
        }      
    } 
    
    /* Retorna o valor da célula especificada   
     * pelos índices da linha e da coluna. */    
    @Override
    public Object getValueAt(int row, int col) {
        // pega a coluna
        Ponto ponto = linhas.get(row);
        
        // Retorna o campo referente a coluna especificada.      
        // Aqui é feito um switch para verificar qual é a coluna      
        // e retornar o campo adequado. As colunas são as mesmas      
        // que foram especificadas no array "colunas".      
        switch (col) {      
        case COL_SEQ:
            return row;
        case COL_X:
            return ponto.getX();  
        case COL_Y:
            return ponto.getY();
        case COL_Z:
            return ponto.getZ();
        default:      
            // Se o índice da coluna não for válido, lança um      
            // IndexOutOfBoundsException (Exceção de índice fora dos limites).      
            // Não foi necessário verificar se o índice da linha é inválido,      
            // pois o próprio ArrayList lança a exceção caso seja inválido.      
            throw new IndexOutOfBoundsException("columnIndex out of bounds");      
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        switch (col) {
            case COL_SEQ:
                return false;
            case COL_X:
                return false;
            case COL_Y:
                return false;
            case COL_Z:
                return false;
            default:
            // Se o índice da coluna não for válido, lança um
                // IndexOutOfBoundsException (Exceção de índice fora dos limites).
                // Não foi necessário verificar se o índice da linha é inválido,
                // pois o próprio ArrayList lança a exceção caso seja inválido.
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }
    
    /**
     * Adiciona um objeto ao model.
     * 
     * @param ponto
     */
    public void add(Ponto ponto){
        this.linhas.add(ponto);
        fireTableDataChanged();
    }
    
    /**
     * Adiciona uma lista de objetos ao model.
     * 
     * @param listaPonto
     */
    public void addAll(List<Ponto> listaPonto){
        this.linhas.addAll(listaPonto);
        fireTableDataChanged();
    }

    /**
     * Remove o objeto da lista
     * 
     * @param ponto 
     */
    public void remove(Ponto ponto){
        this.linhas.remove(ponto);
        fireTableDataChanged();
    }

    /**
     * Remove o indice passado por parâmetro e retorna o objeto removido.
     * 
     * @param row
     * @return Ponto removido do model.
     */
    public Ponto remove(int row){
        Ponto ponto = this.linhas.remove(row);
        fireTableDataChanged();
        return ponto;
    }

    /**
     * Recupera a lista de objetos do model.
     * 
     * @return List
     */
    public List<Ponto> getData() {
        return linhas;
    }
    
    /**
     * Recupera a lista de objetos do model.
     * 
     * @return Set 
     */
    @SuppressWarnings("unchecked")
    public Set<Ponto> getPonto(){
        Set<Ponto> listaPontos = new HashSet<>(linhas);
        return listaPontos;
    }

    /**
     * altera a lista de objetos do model.
     * 
     * @param linhas 
     */
    public void setData(List<Ponto> linhas) {
        this.linhas = linhas;
        this.fireTableDataChanged();
    }

    public void setData(Set<Ponto> linhas) {
        //Limpa a lista
        this.linhas.clear();
        
        //cria uma nova lista a partir do constituicao indicado.
        if (linhas != null) {
            for (Ponto ponto : linhas) {
                this.linhas.add(ponto);
            }
        }
        //atualiza a tabela.
        fireTableDataChanged();
    }

    public void clear(){
        this.linhas.clear();
        fireTableDataChanged();
    };
}
