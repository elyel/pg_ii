package br.com.elyel.pgii.trabalho01.visualizacao;

import br.com.elyel.pgii.trabalho01.classes.Objeto2D;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class ObjetoTableModel  extends AbstractTableModel{
    private static final long serialVersionUID = 210721851360409637L;
    
    /* Lista de Objeto2Ds que representam as linhas */
    private List<Objeto2D> linhas;
    
    /* indice de colunas */
    public static final int COL_TIPO = 0;
    public static final int COL_NOME = 1;
    
    /* Array de Strings com o nome das colunas. */ 
    private final String[] colunas = new String[]{
        "Tipo", "Nome"
    };
    
    /* cria uma Objeto2DsTableModel Vazia */
    public ObjetoTableModel() {
    }
    
    /* cria uma Objeto2DsTableModel com dados */
    public ObjetoTableModel(List<Objeto2D> linhas) {
        this.linhas = linhas;
    }
    
    /* Retorna a quantidade de colunas */
    @Override
    public int getRowCount() {
        /* retorna o tamanho do array de linhas */
        try{
            return this.linhas.size();
        }catch(Exception e){
            return 0;
        }
    }

    /* Retorna a quantidade de linhas. */ 
    @Override
    public int getColumnCount() {
        // Retorna o tamanho da lista de dados
        return this.colunas.length;
    }

    /* Retorna o nome da coluna no índice especificado.   
     * Este método é usado pela JTable para saber o texto do cabeçalho. */      
    @Override      
    public String getColumnName(int columnIndex) {      
        // Retorna o conteúdo do Array que possui o nome das colunas      
        // no índice especificado.      
        return colunas[columnIndex];      
    };      
    
    
      
    /* Retorna a classe dos elementos da coluna especificada.   
     * Este método é usado pela JTable na hora de definir o editor da célula. */      
    @Override      
    public Class<?> getColumnClass(int columnIndex) {      
        // Retorna a classe referente a coluna especificada.      
        // Aqui é feito um switch para verificar qual é a coluna      
        // e retornar o tipo adequado. As colunas são as mesmas      
        // que foram especificadas no array "colunas".      
        switch (columnIndex) {
            case COL_TIPO:
                return String.class;
            case COL_NOME:
                return String.class;
            default:
                // Se o índice da coluna não for válido, lança um      
                // IndexOutOfBoundsException (Exceção de índice fora dos limites).      
                // Não foi necessário verificar se o índice da linha é inválido,      
                // pois o próprio ArrayList lança a exceção caso seja inválido.      
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }
    
    /* Retorna o valor da célula especificada   
     * pelos índices da linha e da coluna. */    
    @Override
    public Object getValueAt(int row, int col) {
        // pega a coluna
        Objeto2D objeto2D = linhas.get(row);
        
        // Retorna o campo referente a coluna especificada.      
        // Aqui é feito um switch para verificar qual é a coluna      
        // e retornar o campo adequado. As colunas são as mesmas      
        // que foram especificadas no array "colunas".      
        switch (col) {      
        case COL_TIPO:
            return objeto2D.getTipo();
        case COL_NOME:
            return objeto2D.getNome();  
        default:      
            // Se o índice da coluna não for válido, lança um      
            // IndexOutOfBoundsException (Exceção de índice fora dos limites).      
            // Não foi necessário verificar se o índice da linha é inválido,      
            // pois o próprio ArrayList lança a exceção caso seja inválido.      
            throw new IndexOutOfBoundsException("columnIndex out of bounds");      
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        switch (col) {
            case COL_TIPO:
                return false;
            case COL_NOME:
                return false;
            default:
            // Se o índice da coluna não for válido, lança um
                // IndexOutOfBoundsException (Exceção de índice fora dos limites).
                // Não foi necessário verificar se o índice da linha é inválido,
                // pois o próprio ArrayList lança a exceção caso seja inválido.
                throw new IndexOutOfBoundsException("columnIndex out of bounds");
        }
    }
    
    /**
     * Adiciona um objeto2D ao model.
     * 
     * @param objeto2D
     */
    public void add(Objeto2D objeto2D){
        this.linhas.add(objeto2D);
        fireTableDataChanged();
    }
    
    /**
     * Adiciona uma lista de objeto2Ds ao model.
     * 
     * @param listaObjeto2D
     */
    public void addAll(List<Objeto2D> listaObjeto2D){
        this.linhas.addAll(listaObjeto2D);
        fireTableDataChanged();
    }

    /**
     * Remove o objeto2D da lista
     * 
     * @param objeto2D 
     */
    public void remove(Objeto2D objeto2D){
        this.linhas.remove(objeto2D);
        fireTableDataChanged();
    }

    /**
     * Remove o indice passado por parâmetro e retorna o objeto2D removido.
     * 
     * @param row
     * @return Objeto2D removido do model.
     */
    public Objeto2D remove(int row){
        Objeto2D objeto2D = this.linhas.remove(row);
        fireTableDataChanged();
        return objeto2D;
    }

    /**
     * Recupera a lista de objeto2Ds do model.
     * 
     * @return List
     */
    public List<Objeto2D> getData() {
        return linhas;
    }
    
    /**
     * Recupera a lista de objeto2Ds do model.
     * 
     * @return Set 
     */
    @SuppressWarnings("unchecked")
    public Set<Objeto2D> getObjeto2D(){
        Set<Objeto2D> listaObjeto2Ds = new HashSet<>(linhas);
        return listaObjeto2Ds;
    }

    /**
     * altera a lista de objeto2Ds do model.
     * 
     * @param linhas 
     */
    public void setData(List<Objeto2D> linhas) {
        this.linhas = linhas;
        this.fireTableDataChanged();
    }

    public void setData(Set<Objeto2D> linhas) {
        //Limpa a lista
        this.linhas.clear();
        
        //cria uma nova lista a partir do constituicao indicado.
        if (linhas != null) {
            for (Objeto2D objeto2D : linhas) {
                this.linhas.add(objeto2D);
            }
        }
        //atualiza a tabela.
        fireTableDataChanged();
    }

    public void clear(){
        this.linhas.clear();
        fireTableDataChanged();
    };
}
