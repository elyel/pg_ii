package br.com.elyel.pgii.trabalho01.enums;

/**
 *
 * @author Elfab
 */
public enum TipoEscalonamento {
    SIMPLES, EM_RELACAO_A_ORIGEM, EM_RELACAO_AO_CENTRO_DO_OBJETO
}
