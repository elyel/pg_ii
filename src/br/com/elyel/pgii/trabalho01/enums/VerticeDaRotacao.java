package br.com.elyel.pgii.trabalho01.enums;

/**
 *
 * @author Elfab
 */
public enum VerticeDaRotacao {
    SOBRE_ORIGEM, SOBRE_UM_PONTO_DO_OBJETO, SOBRE_O_CENTRO_DO_OBJETO
}
