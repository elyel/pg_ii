package br.com.elyel.pgii.trabalho01.enums;

/**
 *
 * @author Elfab
 */
public enum TipoRotacao {
    HORARIO, ANTI_HORARIO
}
