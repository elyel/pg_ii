
package br.com.elyel.pgii.trabalho01.classes;



/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class ViewPort {
    private int maxX;
    private int maxY;
    private int minX;
    private int minY;
    
    private Window window;
    

    public ViewPort(Window window, int maxX, int maxY, int minX, int minY) {
        this.window = window;
        this.maxX = maxX;
        this.maxY = maxY;
        this.minX = minX;
        this.minY = minY;
    }

    public ViewPort(int maxX, int maxY, int minX, int minY) {
        this.maxX = maxX;
        this.maxY = maxY;
        this.minX = minX;
        this.minY = minY;
        this.window = new Window(maxX, minX, maxY, minY);
    }
    
    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public int getMaxX() {
        return maxX;
    }

    public void setMaxX(int maxX) {
        this.maxX = maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setMaxY(int maxY) {
        this.maxY = maxY;
    }

    public int getMinX() {
        return minX;
    }

    public void setMinX(int minX) {
        this.minX = minX;
    }

    public int getMinY() {
        return minY;
    }

    public void setMinY(int minY) {
        this.minY = minY;
    }
    
}
