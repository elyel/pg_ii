
package br.com.elyel.pgii.trabalho01.classes;

import br.com.elyel.pgii.trabalho01.enums.TipoEscalonamento;
import br.com.elyel.pgii.trabalho01.enums.TipoRotacao;
import br.com.elyel.pgii.trabalho01.enums.VerticeDaRotacao;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class Objeto2D{

    public enum Tipo {PONTO, LINHA, POLIGONO};
    
    private String nome;
    private Tipo tipo;
    private List<Ponto> coordenadas = new LinkedList<>();
    
    public Objeto2D(String nome) {
        this.nome = nome;
        this.tipo = Tipo.LINHA;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Ponto> getCoordenadas() {
        return coordenadas;
    }
    
    public void setCoordenadas(List<Ponto> coordenadas) {
        this.coordenadas = coordenadas;
    }
    
    public void addPonto(Ponto ponto){
        this.coordenadas.add(ponto);
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Ponto getPontoMaisProximoOrigem() {
        Ponto origem = new Ponto(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);
        Ponto pontoMaisProximoOrigem = null;
        
        double menorDistancia = Double.MAX_VALUE;
        for(Ponto p : getCoordenadas()){
            if(origem.getDistanciaPara(p) < menorDistancia){
                menorDistancia = origem.getDistanciaPara(p);
                pontoMaisProximoOrigem = p;
            }
        }
        
        return pontoMaisProximoOrigem;
    }

    public Ponto getCentro(){
        if(getCoordenadas().size() > 0);
        
        BigDecimal xCentro = BigDecimal.ZERO;
        BigDecimal yCentro = BigDecimal.ZERO;
        BigDecimal zCentro = BigDecimal.ZERO;
        BigDecimal wCentro = BigDecimal.ZERO;
        
        for(Ponto p : getCoordenadas()){
            xCentro = xCentro.add(p.getX());
            yCentro = yCentro.add(p.getY());
            zCentro = zCentro.add(p.getZ());
            wCentro = wCentro.add(p.getW());
        }
        
        BigDecimal divisor = new BigDecimal(getCoordenadas().size());
        xCentro = xCentro.divide(divisor).setScale(4, RoundingMode.HALF_UP);
        yCentro = yCentro.divide(divisor).setScale(4, RoundingMode.HALF_UP);
        zCentro = zCentro.divide(divisor).setScale(4, RoundingMode.HALF_UP);
        wCentro = wCentro.divide(divisor).setScale(4, RoundingMode.HALF_UP);
        
        return new Ponto(xCentro, yCentro, zCentro, wCentro);
    }
}
