
package br.com.elyel.pgii.trabalho01.classes;

import br.com.elyel.pgii.trabalho01.enums.TipoEscalonamento;
import br.com.elyel.pgii.trabalho01.enums.TipoRotacao;
import br.com.elyel.pgii.trabalho01.enums.VerticeDaRotacao;
import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class Matriz4x4 {

    private Vetor vetor1;
    private Vetor vetor2;
    private Vetor vetor3;
    private Vetor vetor4;

    public Matriz4x4() {
        this(
                new Vetor(0, 0, 0, 0),
                new Vetor(0, 0, 0, 0),
                new Vetor(0, 0, 0, 0),
                new Vetor(0, 0, 0, 0)
        );
    }

    public Matriz4x4(Vetor vetor1, Vetor vetor2, Vetor vetor3, Vetor vetor4) {
        this.vetor1 = vetor1;
        this.vetor2 = vetor2;
        this.vetor3 = vetor3;
        this.vetor4 = vetor4;
    }

    public Vetor getVetor1() {
        return vetor1;
    }

    public void setVetor1(Vetor vetor1) {
        this.vetor1 = vetor1;
    }

    public Vetor getVetor2() {
        return vetor2;
    }

    public void setVetor2(Vetor vetor2) {
        this.vetor2 = vetor2;
    }

    public Vetor getVetor3() {
        return vetor3;
    }

    public void setVetor3(Vetor vetor3) {
        this.vetor3 = vetor3;
    }

    public Vetor getVetor4() {
        return vetor4;
    }

    public void setVetor4(Vetor vetor4) {
        this.vetor4 = vetor4;
    }
    
    
    
    public static Matriz4x4 getMatrizIdentidade(){
        return new Matriz4x4(
                new Vetor(1, 0, 0, 0), 
                new Vetor(0, 1, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
    }
    
    public static Matriz4x4 getCopia(Matriz4x4 matriz){
        return new Matriz4x4(
                matriz.getVetor1(), 
                matriz.getVetor2(), 
                matriz.getVetor3(), 
                matriz.getVetor4());
    }
    
    public static Matriz4x4 getTransposta(Matriz4x4 matriz4x4){
        
        Vetor linha1_inv = new Vetor(
                matriz4x4.getVetor1().getX(), 
                matriz4x4.getVetor2().getX(), 
                matriz4x4.getVetor3().getX(),
                matriz4x4.getVetor4().getX()
        );
        
        Vetor linha2_inv = new Vetor(
                matriz4x4.getVetor1().getY(), 
                matriz4x4.getVetor2().getY(), 
                matriz4x4.getVetor3().getY(),
                matriz4x4.getVetor4().getY()
        );
        
        Vetor linha3_inv = new Vetor(
                matriz4x4.getVetor1().getZ(), 
                matriz4x4.getVetor2().getZ(), 
                matriz4x4.getVetor3().getZ(),
                matriz4x4.getVetor4().getZ()
        );
        
        Vetor linha4_inv = new Vetor(
                matriz4x4.getVetor1().getW(), 
                matriz4x4.getVetor2().getW(), 
                matriz4x4.getVetor3().getW(),
                matriz4x4.getVetor4().getW()
        );
        
        
        return new Matriz4x4(linha1_inv, linha2_inv, linha3_inv, linha4_inv);
    }
    
    public static Matriz4x4 getMatrizRotacaoHorario(double agulo){
        double cosAngulo = Math.cos( (Math.PI/180) * agulo );
        double senAngulo = Math.sin( (Math.PI/180) * agulo );
        
        return new Matriz4x4(
                new Vetor(cosAngulo, (-senAngulo), 0, 0), 
                new Vetor(senAngulo, cosAngulo, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
    }

    public static Matriz4x4 getMatrizRotacaoHorario(double agulo, String eixo) {
        double cosAngulo = Math.cos((Math.PI / 180) * agulo);
        double senAngulo = Math.sin((Math.PI / 180) * agulo);

        switch (eixo) {
            case "X":
                return new Matriz4x4(
                        new Vetor(1, 0, 0, 0),
                        new Vetor(0, cosAngulo, (-senAngulo), 0),
                        new Vetor(0, senAngulo, cosAngulo, 0),
                        new Vetor(0, 0, 0, 1)
                );
            case "Y":
                return new Matriz4x4(
                        new Vetor(cosAngulo, 0, senAngulo, 0),
                        new Vetor(0, 1, 0, 0),
                        new Vetor((-senAngulo), 0, cosAngulo, 0),
                        new Vetor(0, 0, 0, 1)
                );
            //Z
            default:
                return new Matriz4x4(
                        new Vetor(cosAngulo, (-senAngulo), 0, 0),
                        new Vetor(senAngulo, cosAngulo, 0, 0),
                        new Vetor(0, 0, 1, 0),
                        new Vetor(0, 0, 0, 1)
                );
        }
    }
    
    public static Matriz4x4 getMatrizRotacaoAntiHorario(double agulo){
        double cosAngulo = Math.cos( (Math.PI/180) * agulo );
        double senAngulo = Math.sin( (Math.PI/180) * agulo );
        
        return new Matriz4x4(
                new Vetor(cosAngulo, senAngulo, 0, 0), 
                new Vetor(-senAngulo, cosAngulo, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
    }
    
        public static Matriz4x4 getMatrizRotacaoAntiHorario(double agulo, String eixo) {
        double cosAngulo = Math.cos((Math.PI / 180) * agulo);
        double senAngulo = Math.sin((Math.PI / 180) * agulo);

        switch (eixo) {
            case "X":
            case "x":
                return new Matriz4x4(
                        new Vetor(1, 0, 0, 0),
                        new Vetor(0, cosAngulo, senAngulo, 0),
                        new Vetor(0, -senAngulo, cosAngulo, 0),
                        new Vetor(0, 0, 0, 1)
                );
            case "Y":
            case "y":
                return new Matriz4x4(
                        new Vetor(cosAngulo, (-senAngulo), 0, 0),
                        new Vetor(0, 1, 0, 0),
                        new Vetor(senAngulo, 0, cosAngulo, 0),
                        new Vetor(0, 0, 0, 1)
                );
            //Z
            default:
                return new Matriz4x4(
                        new Vetor(cosAngulo, (senAngulo), 0, 0),
                        new Vetor(-senAngulo, cosAngulo, 0, 0),
                        new Vetor(0, 0, 1, 0),
                        new Vetor(0, 0, 0, 1)
                );
        }
    }
    
    public static Matriz4x4 getMatrizTranslacao(double deslocamentoX, double deslocamentoY, double deslocamentoZ){
        return new Matriz4x4(
                new Vetor(1, 0, 0, 0), 
                new Vetor(0, 1, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(deslocamentoX, deslocamentoY, deslocamentoZ, 1)
        );
    }
    
    public static Matriz4x4 getMatrizTranslacao(Vetor vetorDeslocamento){
        return new Matriz4x4(
                new Vetor(1, 0, 0, 0), 
                new Vetor(0, 1, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(vetorDeslocamento.getX().doubleValue(), vetorDeslocamento.getY().doubleValue(), vetorDeslocamento.getZ().doubleValue(), 1)
        );
    }
    
    public static Matriz4x4 getMatrizEscalonamentoSimples(double escalonamentoX, double escalonamentoY, double escalonamentoZ){
        return new Matriz4x4(
                new Vetor(escalonamentoX, 0, 0, 0), 
                new Vetor(0, escalonamentoY, 0, 0), 
                new Vetor(0, 0, escalonamentoZ, 0), 
                new Vetor(0, 0, 0, 1)
        );
    }
    
    public static Matriz4x4 getMatrizEscalonamentoSimples(Vetor vetorEscalonamento){
        return new Matriz4x4(
                new Vetor(vetorEscalonamento.getX().doubleValue(), 0, 0, 0), 
                new Vetor(0, vetorEscalonamento.getY().doubleValue(), 0, 0), 
                new Vetor(0, 0, vetorEscalonamento.getZ().doubleValue(), 0), 
                new Vetor(0, 0, 0, 1)
        );
    }
    
    public static Matriz4x4 getMatrizEscalonamentoEmRelacaoOrigem(Vetor vetorEscalonamento, Objeto2D objeto2D) {
        Ponto pontoMaisProximoOrigem = objeto2D.getPontoMaisProximoOrigem();
        
        pontoMaisProximoOrigem.mult(-1);// Inverte os valores de xyz para gerar a transpacao para origem
        Matriz4x4 matrizTranslacaoOrigem = getMatrizTranslacao(pontoMaisProximoOrigem);
        
        pontoMaisProximoOrigem.mult(-1);// Inverte os valores de xyz para gerar a transpacao inversa
        Matriz4x4 matrizTranslacaoRetorno = getMatrizTranslacao(pontoMaisProximoOrigem);
        
        Matriz4x4 matrizEscalonamento = Matriz4x4.getMatrizEscalonamentoSimples(vetorEscalonamento);
        
        return matrizTranslacaoOrigem.multiplicar(matrizEscalonamento).multiplicar(matrizTranslacaoRetorno);
        
    }
    
    private static Matriz4x4 getMatrizEscalonamentoEmRelacaoAoCentro(Vetor vetorEscalonamento, Objeto2D objeto2D) {
        Ponto centroObjeto = objeto2D.getCentro();
        
        centroObjeto.mult(-1);// Inverte os valores de xyz para gerar a transpacao para origem
        Matriz4x4 matrizTranslacaoOrigem = getMatrizTranslacao(centroObjeto);
        
        centroObjeto.mult(-1);// Inverte os valores de xyz para gerar a transpacao inversa
        Matriz4x4 matrizTranslacaoRetorno = getMatrizTranslacao(centroObjeto);
        
        Matriz4x4 matrizEscalonamento = Matriz4x4.getMatrizEscalonamentoSimples(vetorEscalonamento);
        
        return matrizTranslacaoOrigem.multiplicar(matrizEscalonamento).multiplicar(matrizTranslacaoRetorno);
    }
    
    private static Matriz4x4 getMatrizRotacao(Objeto2D objeto2D, BigDecimal anguloRotacao, TipoRotacao tipoRotacao, VerticeDaRotacao verticeDaRotacao, String eixoRotacao) {
        
        Matriz4x4 matrizDeslocamentoRotacao = getMatrizIdentidade();
        Matriz4x4 matrizRetornoRotacao = getMatrizIdentidade();
        switch(verticeDaRotacao){
            case SOBRE_ORIGEM:
                matrizDeslocamentoRotacao = getMatrizIdentidade();
                matrizRetornoRotacao = getMatrizIdentidade();
                break;
            case SOBRE_O_CENTRO_DO_OBJETO:
                Ponto deslocamentoRotacao = objeto2D.getCentro();
                deslocamentoRotacao.mult(-1);
                matrizDeslocamentoRotacao = getMatrizTranslacao(deslocamentoRotacao);
                deslocamentoRotacao.mult(-1);
                matrizRetornoRotacao = getMatrizTranslacao(deslocamentoRotacao);
                break;
            case SOBRE_UM_PONTO_DO_OBJETO:
                Ponto deslocamentoRotacao1 = objeto2D.getPontoMaisProximoOrigem();
                deslocamentoRotacao1.mult(-1);
                matrizDeslocamentoRotacao = getMatrizTranslacao(deslocamentoRotacao1);
                deslocamentoRotacao1.mult(-1);
                matrizRetornoRotacao = getMatrizTranslacao(deslocamentoRotacao1);
                break;
        }
        
        Matriz4x4 matrizRotacao;
        if(tipoRotacao == TipoRotacao.HORARIO){
            matrizRotacao = getMatrizRotacaoHorario(anguloRotacao.doubleValue(), eixoRotacao);
        } else {
            matrizRotacao = getMatrizRotacaoAntiHorario(anguloRotacao.doubleValue(), eixoRotacao);
        }
        
       return matrizDeslocamentoRotacao.multiplicar(matrizRotacao).multiplicar(matrizRetornoRotacao);
    }

    private static Matriz4x4 getMatrizEscalonamento(Objeto2D objeto2D, Vetor vetorEscalonamento, TipoEscalonamento tipoEscalonamento) {
        switch (tipoEscalonamento) {
            case SIMPLES:
                return getMatrizEscalonamentoSimples(vetorEscalonamento);
            case EM_RELACAO_A_ORIGEM:
                return getMatrizEscalonamentoEmRelacaoOrigem(vetorEscalonamento, objeto2D);
            case EM_RELACAO_AO_CENTRO_DO_OBJETO:
                return getMatrizEscalonamentoEmRelacaoAoCentro(vetorEscalonamento, objeto2D);
            default:
                return getMatrizIdentidade();
        }

    }
    
    private static Matriz4x4 getMatrizReflexao(boolean refletirEixoX, boolean refletirEixoY, boolean refletirEixoZ) {
        return new Matriz4x4(
                new Vetor((refletirEixoX ? -1 : 1), 0, 0, 0), 
                new Vetor(0, (refletirEixoY ? -1 : 1), 0, 0), 
                new Vetor(0, 0, (refletirEixoZ ? -1 : 1), 0), 
                new Vetor(0, 0, 0, 1)
        );
    }

    private static Matriz4x4 getMatrizCisalhamento(Vetor vetorCisalhamento) {
        return new Matriz4x4(
                new Vetor(1, vetorCisalhamento.getX().doubleValue(), 0, 0),
                new Vetor(vetorCisalhamento.getY().doubleValue(), 1, 0, 0),
                new Vetor(0, 0, 1, 0),
                new Vetor(0, 0, 0, 1)
        );
    }

 
    /**
     * Cria uma matriz homogênea para realizar as operações de translacao, 
     * escalonamento e rotação, caso alguma operação seja desnecessária, basta 
     * informar null no parâmetro referente a operação em questão.
     * 
     * @param vetorTranslacao
     * @param vetorEscalonamento
     * @param tipoEscalonamento
     * @param anguloRotacao
     * @param verticeDaRotacao
     * @param tipoRotacao
     * @param eixoRotacao (X, Y ou Z)
     * @param refletirEixoX
     * @param refletirEixoY
     * @param refletirEixoZ
     * @param vetorCisalhamento
     * @param objeto2D
     * 
     * @return 
     *     Retorna uma matriz4x4, com as operações de translação, escalonamento
     *     e rotação correspondente aos vetores informados por parâmetro.
     *       
     */
    public static Matriz4x4 getMatrizHomogenea(
            Vetor vetorTranslacao,
            Vetor vetorEscalonamento,
            TipoEscalonamento tipoEscalonamento,
            BigDecimal anguloRotacao,
            TipoRotacao tipoRotacao,
            VerticeDaRotacao verticeDaRotacao,
            String eixoRotacao,
            boolean refletirEixoX,
            boolean refletirEixoY,
            boolean refletirEixoZ,
            Vetor vetorCisalhamento,
            Objeto2D objeto2D
    ){
        Matriz4x4 matrizTranslacao = getMatrizTranslacao(vetorTranslacao);
        Matriz4x4 matrizEscalonamento = getMatrizEscalonamento(objeto2D, vetorEscalonamento, tipoEscalonamento);
        Matriz4x4 matrizRotacao = getMatrizRotacao(objeto2D, anguloRotacao, tipoRotacao, verticeDaRotacao, eixoRotacao);
        Matriz4x4 matrizEspelhamento = getMatrizReflexao(refletirEixoX, refletirEixoY, refletirEixoZ);
        Matriz4x4 matrizCisalhamento = getMatrizCisalhamento(vetorCisalhamento);
        
        return matrizTranslacao
                .multiplicar(matrizEscalonamento)
                .multiplicar(matrizRotacao)
                .multiplicar(matrizEspelhamento)
                .multiplicar(matrizCisalhamento);
    }
    
    
    public Matriz4x4 multiplicar(Matriz4x4 matrizMultiplicadora){
        
        Vetor linha1 = this.getVetor1();
        Vetor linha2 = this.getVetor2();
        Vetor linha3 = this.getVetor3();
        Vetor linha4 = this.getVetor4();
        
        Vetor coluna1 = new Vetor(
                matrizMultiplicadora.getVetor1().getX(), 
                matrizMultiplicadora.getVetor2().getX(), 
                matrizMultiplicadora.getVetor3().getX(),
                matrizMultiplicadora.getVetor4().getX()
        );
        Vetor coluna2 = new Vetor(
                matrizMultiplicadora.getVetor1().getY(), 
                matrizMultiplicadora.getVetor2().getY(), 
                matrizMultiplicadora.getVetor3().getY(),
                matrizMultiplicadora.getVetor4().getY()
        );
        Vetor coluna3 = new Vetor(
                matrizMultiplicadora.getVetor1().getZ(), 
                matrizMultiplicadora.getVetor2().getZ(), 
                matrizMultiplicadora.getVetor3().getZ(),
                matrizMultiplicadora.getVetor4().getZ()
        );
        Vetor coluna4 = new Vetor(
                matrizMultiplicadora.getVetor1().getW(), 
                matrizMultiplicadora.getVetor2().getW(), 
                matrizMultiplicadora.getVetor3().getW(),
                matrizMultiplicadora.getVetor4().getW()
        );
            
        Vetor resultadoLinha1 = new Vetor(
                linha1.mult(coluna1), linha1.mult(coluna2), linha1.mult(coluna3), linha1.mult(coluna4));
        Vetor resultadoLinha2 = new Vetor(
                linha2.mult(coluna1), linha2.mult(coluna2), linha2.mult(coluna3), linha2.mult(coluna4));
        Vetor resultadoLinha3 = new Vetor(
                linha3.mult(coluna1), linha3.mult(coluna2), linha3.mult(coluna3), linha3.mult(coluna4));
        Vetor resultadoLinha4 = new Vetor(
                linha4.mult(coluna1), linha4.mult(coluna2), linha4.mult(coluna3), linha4.mult(coluna4));
        
        return new Matriz4x4(resultadoLinha1, resultadoLinha2, resultadoLinha3, resultadoLinha4);
    }

    @Override
    public String toString() {
        String s;
        
        s = this.vetor1.getX() + "\t" + this.vetor1.getY() + "\t" + this.vetor1.getZ() + "\t" + this.vetor1.getW() + "\n"
                + this.vetor2.getX() + "\t" + this.vetor2.getY() + "\t" + this.vetor2.getZ() + "\t" + this.vetor2.getW() + "\n"
                + this.vetor3.getX() + "\t" + this.vetor3.getY() + "\t" + this.vetor3.getZ() + "\t" + this.vetor3.getW() + "\n"
                + this.vetor4.getX() + "\t" + this.vetor4.getY() + "\t" + this.vetor4.getZ() + "\t" + this.vetor4.getW() + "\n";
        
        return s;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.vetor1);
        hash = 83 * hash + Objects.hashCode(this.vetor2);
        hash = 83 * hash + Objects.hashCode(this.vetor3);
        hash = 83 * hash + Objects.hashCode(this.vetor4);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Matriz4x4 other = (Matriz4x4) obj;
        if (!Objects.equals(this.vetor1, other.vetor1)) {
            return false;
        }
        if (!Objects.equals(this.vetor2, other.vetor2)) {
            return false;
        }
        if (!Objects.equals(this.vetor3, other.vetor3)) {
            return false;
        }
        if (!Objects.equals(this.vetor4, other.vetor4)) {
            return false;
        }
        return true;
    }
    
    
}
