
package br.com.elyel.pgii.trabalho01.classes;

import br.com.elyel.pgii.trabalho01.enums.TipoEscalonamento;
import br.com.elyel.pgii.trabalho01.enums.TipoRotacao;
import br.com.elyel.pgii.trabalho01.enums.VerticeDaRotacao;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class TransformacaoObjeto {
    private BigDecimal angulo;
    private TipoRotacao tipoRotacao;
    private Vetor escala;
    private TipoEscalonamento tipoEscalonamento;
    private Vetor traslacao;
    private VerticeDaRotacao verticeDaRotacao;
    private String eixoRotacao;
    private boolean refletirEixoX;
    private boolean refletirEixoY;
    private boolean refletirEixoZ;
    private Vetor cisalhamento;
    
    public BigDecimal getAngulo() {
        return angulo;
    }

    public void setAngulo(BigDecimal angulo) {
        this.angulo = angulo;
    }

    public TipoRotacao getTipoRotacao() {
        return tipoRotacao;
    }

    public void setTipoRotacao(TipoRotacao tipoRotacao) {
        this.tipoRotacao = tipoRotacao;
    }

    public Vetor getEscala() {
        return escala;
    }

    public void setEscala(Vetor escala) {
        this.escala = escala;
    }

    public TipoEscalonamento getTipoEscalonamento() {
        return tipoEscalonamento;
    }

    public void setTipoEscalonamento(TipoEscalonamento tipoEscalonamento) {
        this.tipoEscalonamento = tipoEscalonamento;
    }

    public Vetor getTraslacao() {
        return traslacao;
    }

    public void setTraslacao(Vetor traslacao) {
        this.traslacao = traslacao;
    }

    public VerticeDaRotacao getVerticeDaRotacao() {
        return verticeDaRotacao;
    }

    public void setVerticeDaRotacao(VerticeDaRotacao verticeDaRotacao) {
        this.verticeDaRotacao = verticeDaRotacao;
    }

    public boolean isRefletirEixoX() {
        return refletirEixoX;
    }

    public void setRefletirEixoX(boolean refletirEixoX) {
        this.refletirEixoX = refletirEixoX;
    }

    public boolean isRefletirEixoY() {
        return refletirEixoY;
    }

    public void setRefletirEixoY(boolean refletirEixoY) {
        this.refletirEixoY = refletirEixoY;
    }

    public boolean isRefletirEixoZ() {
        return refletirEixoZ;
    }

    public void setRefletirEixoZ(boolean refletirEixoZ) {
        this.refletirEixoZ = refletirEixoZ;
    }

    public Vetor getCisalhamento() {
        return cisalhamento;
    }

    public void setCisalhamento(Vetor cisalhamento) {
        this.cisalhamento = cisalhamento;
    }

    public String getEixoRotacao() {
        return eixoRotacao;
    }

    public void setEixoRotacao(String eixoRotacao) {
        this.eixoRotacao = eixoRotacao;
    }
    
    public void aplicarTransformacaoAosObjetos(List<Objeto2D> listaObjetos){
        Matriz4x4 matrizHomogenea;
        for(Objeto2D objeto2D : listaObjetos) {
            //Carrega a matriz homogenea para a transformação
            matrizHomogenea = getMatrizHomogeneaTransformacaoObjeto(objeto2D);
            
            //Percorre todos os pontos do objeto
            List<Ponto> pontosTransformados = new LinkedList<>();
            for(Ponto ponto : objeto2D.getCoordenadas()){
                //aplica a trasformação no ponto
                pontosTransformados.add(ponto.mult(matrizHomogenea));
            }
            
            //seta as novas coordenadas do objeto.
            objeto2D.setCoordenadas(pontosTransformados);
        }
    }

    private Matriz4x4 getMatrizHomogeneaTransformacaoObjeto(Objeto2D objeto2D) {
        return Matriz4x4.getMatrizHomogenea(
                this.traslacao, 
                this.escala, 
                this.tipoEscalonamento, 
                this.angulo, 
                this.tipoRotacao, 
                this.verticeDaRotacao, 
                this.getEixoRotacao(),
                this.refletirEixoX,
                this.refletirEixoY,
                this.refletirEixoZ,
                this.cisalhamento,
                objeto2D);
    }
}
