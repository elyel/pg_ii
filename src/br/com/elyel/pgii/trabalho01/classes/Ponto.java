
package br.com.elyel.pgii.trabalho01.classes;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 
 Este ponto apenas extende a Vetor, que possui as propriedades x, y, z e
 as operações de subtrair, adicionar, multiplicar,  dividir, normalização e
 magnitude
 */
public class Ponto extends Vetor{

    public Ponto(BigDecimal x, BigDecimal y, BigDecimal z, BigDecimal w) {
        super(x, y, z, w);
    }

    public Ponto(BigDecimal x, BigDecimal y, BigDecimal z) {
        super(x, y, z);
    }

    public Ponto(double x, double y, double z, double w) {
        super(x, y, z, w);
    }

    public Ponto(double x, double y, double z) {
        super(x, y, z);
    }


    @Override
    public Ponto mult(Matriz4x4 matriz4x4) {
        Vetor v = super.mult(matriz4x4); 
        return new Ponto(v.getX(), v.getY(), v.getZ(), v.getW());
    }
    
    public double getDistanciaPara(Ponto p){
        double distanciaX = p.getX().subtract(this.getX()).pow(2, MathContext.DECIMAL32).doubleValue();
        double distanciaY = p.getY().subtract(this.getY()).pow(2, MathContext.DECIMAL32).doubleValue();
        double distanciaZ = p.getZ().subtract(this.getZ()).pow(2, MathContext.DECIMAL32).doubleValue();
        
        double distanciaPI = Math.sqrt((distanciaX + distanciaY+ distanciaZ));
        
        return distanciaPI;
    }
    
    public BigDecimal getDistanciaParaBD(Ponto p){
        return new BigDecimal(getDistanciaPara(p)).setScale(2, RoundingMode.HALF_UP);
    }
    
    
    
}
