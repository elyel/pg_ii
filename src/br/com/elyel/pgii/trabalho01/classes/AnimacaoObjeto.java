
package br.com.elyel.pgii.trabalho01.classes;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class AnimacaoObjeto {
    private TransformacaoObjeto transformacaoObjeto;
    private Objeto2D objeto2d;
    private TimerTask timerTask;

    public AnimacaoObjeto(final TransformacaoObjeto transformacaoObjeto, final Objeto2D objeto2d, long periodoAnimacao) {
        this.transformacaoObjeto = transformacaoObjeto;
        this.objeto2d = objeto2d;
        this.timerTask = new TimerTask() {
            @Override
            public void run() {
                List<Objeto2D> listaObjetos = new LinkedList<>();
                listaObjetos.add(objeto2d);
                transformacaoObjeto.aplicarTransformacaoAosObjetos(listaObjetos);
            }
        };
        
        Timer timer = new Timer(true);
        Date agora = new Date();
        timer.schedule(timerTask, agora, periodoAnimacao);
    }
    
    public TransformacaoObjeto getTransformacaoObjeto() {
        return transformacaoObjeto;
    }

    public void setTransformacaoObjeto(TransformacaoObjeto transformacaoObjeto) {
        this.transformacaoObjeto = transformacaoObjeto;
    }

    public Objeto2D getObjeto2d() {
        return objeto2d;
    }

    public void setObjeto2d(Objeto2D objeto2d) {
        this.objeto2d = objeto2d;
    }

    public TimerTask getTimerTask() {
        return timerTask;
    }

    public void setTimerTask(TimerTask timerTask) {
        this.timerTask = timerTask;
    }
    
    public void animar(){
        timerTask.run();
    }
    
    public void parar(){
        timerTask.cancel();
    }
}
