
package br.com.elyel.pgii.trabalho01.classes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class Vetor {
    
    private BigDecimal x;
    private BigDecimal y;
    private BigDecimal z;
    private BigDecimal w;

    /**
     * Construtor do Vetor 3D principal, recebe 3 variáveis BigDecimal como 
     * parâmetro, indicando as coordenadas x, y e z
     * 
     * @param x
     * @param y
     * @param z 
     * @param w 
     */
    public Vetor(BigDecimal x, BigDecimal y, BigDecimal z, BigDecimal w) {
        this.x = x.setScale(4, RoundingMode.HALF_UP);
        this.y = y.setScale(4, RoundingMode.HALF_UP);
        this.z = z.setScale(4, RoundingMode.HALF_UP);
        this.w = w.setScale(4, RoundingMode.HALF_UP);
    }
    
       /**
     * Construtor do Vetor 3D principal, recebe 3 variáveis BigDecimal como 
     * parâmetro, indicando as coordenadas x, y e z
     * 
     * @param x
     * @param y
     * @param z 
     */
    public Vetor(BigDecimal x, BigDecimal y, BigDecimal z) {
        this(x, y, z, BigDecimal.ONE);
    }
    
    public Vetor getClone(){
        Vetor vClone = new Vetor(this.getX(), this.getY(), this.getZ(), this.getW());
        return vClone;
    }
    
    /**
     * Construtor para o objeto Vetor3D secundário, recebe como parâmetros 3 
     * variáveis double, indicando a coordenada x, y e z que são trasformadas
     * em BigDecimal com escala de 4 casas decimais com a forma de arredondamento
     * metade para cima.
     * 
     * @param x
     * @param y
     * @param z 
     * @param w 
     */
    public Vetor(double x, double y, double z, double w){
        this(new BigDecimal(x).setScale(4, RoundingMode.HALF_UP),
             new BigDecimal(y).setScale(4, RoundingMode.HALF_UP),
             new BigDecimal(z).setScale(4, RoundingMode.HALF_UP),
             new BigDecimal(w).setScale(4, RoundingMode.HALF_UP));
    }
    
    /**
     * Construtor para o objeto Vetor3D secundário, recebe como parâmetros 3 
     * variáveis double, indicando a coordenada x, y e z que são trasformadas
     * em BigDecimal com escala de 4 casas decimais com a forma de arredondamento
     * metade para cima.
     * 
     * @param x
     * @param y
     * @param z 
     */
    public Vetor(double x, double y, double z){
        this(x, y, z, 1.00);
    }
    
    public BigDecimal getX() {
        return x;
    }

    public void setX(BigDecimal x) {
        this.x = x.setScale(4, RoundingMode.HALF_UP);
    }

    public BigDecimal getY() {
        return y;
    }

    public void setY(BigDecimal y) {
        this.y = y.setScale(4, RoundingMode.HALF_UP);
    }

    public BigDecimal getZ() {
        return z;
    }

    public void setZ(BigDecimal z) {
        this.z = z.setScale(4, RoundingMode.HALF_UP);
    }

    public BigDecimal getW() {
        return w;
    }

    public void setW(BigDecimal w) {
        this.w = w;
    }
    
    /**
     * adiciona o vetor (Vetor) informado por parâmetro ao vetor existente.
     * 
     * @param vetor 
     */
    public void add(Vetor vetor){
        this.x = this.x.add(vetor.getX()).setScale(4, RoundingMode.HALF_UP);
        this.y = this.y.add(vetor.getY()).setScale(4, RoundingMode.HALF_UP);
        this.z = this.z.add(vetor.getZ()).setScale(4, RoundingMode.HALF_UP);
        this.w = this.w.add(vetor.getW()).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * Adiciona um valor para o vetor na coordenada X.
     * 
     * @param x 
     */
    public void addX(BigDecimal x){
        this.x = this.x.add(x).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * Adiciona um valor para o vetor na coordenada y.
     * 
     * @param y 
     */
    public void addY(BigDecimal y){
        this.y = this.y.add(y).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * adiciona um valor para o vetor na coordenada Z.
     * 
     * @param z 
     */
    public void addZ(BigDecimal z){
        this.z = this.z.add(z).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * adiciona um valor para o vetor na coordenada Z.
     * 
     * @param w
     */
    public void addW(BigDecimal w){
        this.z = this.z.add(w).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * Subtrai o valor o vetor (Vetor) informado por parâtro.
     * 
     * @param vetor 
     */
    public void sub(Vetor vetor){
        this.x = this.x.subtract(vetor.getX()).setScale(4, RoundingMode.HALF_UP);
        this.y = this.y.subtract(vetor.getY()).setScale(4, RoundingMode.HALF_UP);
        this.z = this.z.subtract(vetor.getZ()).setScale(4, RoundingMode.HALF_UP);
        this.w = this.w.subtract(vetor.getZ()).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * Subtrai um valor da coordenada X do vetor.
     * 
     * @param x 
     */
    public void subX(BigDecimal x){
        this.x = this.x.subtract(x.setScale(4, RoundingMode.HALF_UP));
    }
    
    /**
     * Subtrai o valor informado por parâmetro da coordenada y do vetor.
     * @param y 
     */
    public void subY(BigDecimal y){
        this.y = this.y.subtract(y).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * Subtrai o valor informado por parâmetro da coordenada z do vetor.
     * 
     * @param z 
     */
    public void subZ(BigDecimal z){
        this.z = this.z.subtract(z).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * Subtrai o valor informado por parâmetro da coordenada w do vetor.
     * 
     * @param w 
     */
    public void subW(BigDecimal w){
        this.w = this.w.subtract(w).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * multiplica o vetor corrente pelo escalar informado por parâmetro.
     * 
     * @param escalar 
     */
    public void mult(BigDecimal escalar){
        this.x = this.x.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.y = this.y.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.z = this.z.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.w = this.w.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * multiplica o vetor corrente pelo escalar informado por parâmetro.
     * (Escalar é um real positivo ou negativo)
     * 
     * @param iEscalar 
     */
    public void mult(int iEscalar){
        BigDecimal escalar = new BigDecimal(iEscalar).setScale(4, RoundingMode.HALF_UP);
        this.x = this.x.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.y = this.y.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.z = this.z.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.w = this.w.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * multiplica o vetor corrente pelo escalar informado por parâmetro.
     * 
     * @param dEscalar 
     */
    public void mult(double dEscalar){
        BigDecimal escalar = new BigDecimal(dEscalar).setScale(4, RoundingMode.HALF_UP);
        this.x = this.x.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.y = this.y.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.z = this.z.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
        this.w = this.w.multiply(escalar).setScale(4, RoundingMode.HALF_UP);
    }
    
    
    /**
     * retorna a multiplicação de vetores
     * 
     * @param vetor3D
     * @return 
     */
    public BigDecimal mult(Vetor vetor3D){
        
        double dx_u = this.getX().doubleValue();
        double dy_u = this.getY().doubleValue();
        double dz_u = this.getZ().doubleValue();
        double dw_u = this.getW().doubleValue();
        
        double dx_v = vetor3D.getX().doubleValue();
        double dy_v = vetor3D.getY().doubleValue();
        double dz_v = vetor3D.getZ().doubleValue();
        double dw_v = vetor3D.getW().doubleValue();
        
        double dx = dx_u*dx_v;
        double dy = dy_u*dy_v;
        double dz = dz_u*dz_v;
        double dw = dw_u*dw_v;
        double dtotal = dx+dy+dz+dw;
        
        
        return new BigDecimal(dtotal).setScale(4, RoundingMode.HALF_UP);
    }
    
    public Vetor mult(Matriz4x4 matriz4x4) {
        Vetor vetorResultado;

        vetorResultado = new Vetor(
                this.x.multiply(matriz4x4.getVetor1().getX())
                .add(this.y.multiply(matriz4x4.getVetor2().getX()))
                .add(this.z.multiply(matriz4x4.getVetor3().getX()))
                .add(this.w.multiply(matriz4x4.getVetor4().getX()))
                .setScale(4, RoundingMode.HALF_UP),
                this.x.multiply(matriz4x4.getVetor1().getY())
                .add(this.y.multiply(matriz4x4.getVetor2().getY()))
                .add(this.z.multiply(matriz4x4.getVetor3().getY()))
                .add(this.w.multiply(matriz4x4.getVetor4().getY()))
                .setScale(4, RoundingMode.HALF_UP),
                this.x.multiply(matriz4x4.getVetor1().getZ())
                .add(this.y.multiply(matriz4x4.getVetor2().getZ()))
                .add(this.z.multiply(matriz4x4.getVetor3().getZ()))
                .add(this.w.multiply(matriz4x4.getVetor4().getZ()))
                .setScale(4, RoundingMode.HALF_UP),
                this.x.multiply(matriz4x4.getVetor1().getW())
                .add(this.y.multiply(matriz4x4.getVetor2().getW()))
                .add(this.z.multiply(matriz4x4.getVetor3().getW()))
                .add(this.w.multiply(matriz4x4.getVetor4().getW()))
                .setScale(4, RoundingMode.HALF_UP)
        );
        return vetorResultado;
    }

    /**
     * Divide um escalar pelo vetor corrente.
     * 
     * @param escalar 
     */
    public void div(BigDecimal escalar){
        escalar = escalar.setScale(4, RoundingMode.HALF_UP);

        double dx = this.getX().doubleValue();
        double dy = this.getY().doubleValue();
        double dz = this.getZ().doubleValue();
        
        double xx = dx/escalar.doubleValue();
        double yy = dy/escalar.doubleValue();
        double zz = dz/escalar.doubleValue();
        
        this.x = new BigDecimal(xx).setScale(4, RoundingMode.HALF_UP);
        this.y = new BigDecimal(yy).setScale(4, RoundingMode.HALF_UP);
        this.z = new BigDecimal(zz).setScale(4, RoundingMode.HALF_UP);
    }
    /**
     * Divide um escalar pelo vetor corrente.
     * 
     * @param dEscalar 
     */
    public void div(double dEscalar){
        BigDecimal escalar = new BigDecimal(dEscalar).setScale(4, RoundingMode.HALF_UP);
        this.x = this.x.divide(escalar).setScale(4, RoundingMode.HALF_UP);
        this.y = this.y.divide(escalar).setScale(4, RoundingMode.HALF_UP);
        this.z = this.z.divide(escalar).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * Divide um escalar pelo vetor corrente.
     * 
     * @param iEscalar 
     */
    public void div(int iEscalar){
        BigDecimal escalar = new BigDecimal(iEscalar).setScale(4, RoundingMode.HALF_UP);
        this.x = this.x.divide(escalar).setScale(4, RoundingMode.HALF_UP);
        this.y = this.y.divide(escalar).setScale(4, RoundingMode.HALF_UP);
        this.z = this.z.divide(escalar).setScale(4, RoundingMode.HALF_UP);
    }
    
    /**
     * Retorna a norma (Magnitude, vetor unitário) do vetor corrente.
     * 
     * ||v|| = (raiz(x²+y²+z²))
     * 
     * @return 
     */
    public BigDecimal getNorma(){
        BigDecimal quadX = this.x.pow(2).setScale(4, RoundingMode.HALF_UP);
        BigDecimal quadY = this.y.pow(2).setScale(4, RoundingMode.HALF_UP);
        BigDecimal quadZ = this.z.pow(2).setScale(4, RoundingMode.HALF_UP);
        BigDecimal somaQuads = quadX.add(quadY).add(quadZ).setScale(4, RoundingMode.HALF_UP);
        return new BigDecimal(Math.sqrt(somaQuads.doubleValue())).setScale(4, RoundingMode.HALF_UP);   
    }
    
    /**
     * Retorna a norma (Magnitude, vetor unitário) do vetor corrente.
     * 
     * ||v|| = (raiz(x²+y²+z²))
     * 
     * @return 
     */
    public BigDecimal getMagnitude(){
        return this.getNorma();
    }
    
    /**
     * Normaliza o vetor corrente, criando um vetor unitário com base em 
     * suas coordenadas correntes.
     * 
     * v = v / ||v|| -> (x, y, z) / (raiz(x²+y²+z²))
     * 
     */
    public void normalizar(){
        BigDecimal magnitude = this.getMagnitude();
        magnitude.setScale(4, RoundingMode.HALF_UP);
        if(magnitude.compareTo(BigDecimal.ZERO) > 0){
            this.div(magnitude);
        }
    }
    
    public BigDecimal getAngulo(Vetor vetor3D){
        
        double uv = this.mult(vetor3D).doubleValue();
        double normaU = this.getNorma().doubleValue();
        double normaV = vetor3D.getNorma().doubleValue();
        
        double angulo = uv/(normaU*normaV);
        double menorAngulo = Math.acos(angulo);
        
        return new BigDecimal(menorAngulo).setScale(4, RoundingMode.HALF_UP);
    }
    
    public BigDecimal getRadianos(Vetor vetor3D){
        double radianos = Math.toRadians(this.getAngulo(vetor3D).doubleValue());
        return new BigDecimal(radianos).setScale(4, RoundingMode.HALF_UP);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.x);
        hash = 89 * hash + Objects.hashCode(this.y);
        hash = 89 * hash + Objects.hashCode(this.z);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vetor other = (Vetor) obj;
        if (!Objects.equals(this.x, other.x)) {
            return false;
        }
        if (!Objects.equals(this.y, other.y)) {
            return false;
        }
        return Objects.equals(this.z, other.z);
    }

    @Override
    public String toString() {
        return "Vetor3D{" + "x=" + x + ", y=" + y + ", z=" + z + '}';
    }
    
    
}
