
package br.com.elyel.pgii.trabalho01.classes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Elyel Rubens da Rosa - ERR
 */
public class Window {
    private BigDecimal maxX;
    private BigDecimal minX;
    private BigDecimal maxY;
    private BigDecimal minY;
    
    private Objeto2D linhaX;
    private Objeto2D linhaY;
    
    private List<Objeto2D> objetos = new LinkedList<>();
    
    private BigDecimal zoom;
    private Ponto deslocamento;

    public Window(BigDecimal maxX, BigDecimal minX, BigDecimal maxY, BigDecimal minY) {
        this.maxX = maxX.setScale(4);
        this.minX = minX.setScale(4);
        this.maxY = maxY.setScale(4);
        this.minY = minY.setScale(4);
        BigDecimal dois = new BigDecimal(2.00);
        
        linhaX = new Objeto2D("linhaX");
        linhaX.setTipo(Objeto2D.Tipo.LINHA);
        linhaX.addPonto(new Ponto(minX, (maxY.subtract(minY).divide(dois).add(minY)), BigDecimal.ZERO));
        linhaX.addPonto(new Ponto(maxX, (maxY.subtract(minY).divide(dois).add(minY)), BigDecimal.ZERO));
        
        linhaY = new Objeto2D("linhaY");
        linhaY.setTipo(Objeto2D.Tipo.LINHA);
        linhaY.addPonto(new Ponto((maxX.subtract(minX).divide(dois).add(minX)), minY, BigDecimal.ZERO));
        linhaY.addPonto(new Ponto((maxX.subtract(minX).divide(dois).add(minX)), maxY, BigDecimal.ZERO));
        
        
        this.zoom = BigDecimal.ONE.setScale(3, RoundingMode.CEILING);
        this.zoom.setScale(3, RoundingMode.CEILING);
        this.deslocamento = new Ponto(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);
    }
    
    public Window(int maxX, int minX, int maxY, int minY) {
        this(new BigDecimal(maxX), new BigDecimal(minX), new BigDecimal(maxY), new BigDecimal(minY));
    }

    public BigDecimal getMaxX() {
        return maxX.divide(this.zoom, 3, RoundingMode.HALF_UP).add(deslocamento.getX());
    }

    public void setMaxX(BigDecimal maxX) {
        this.maxX = maxX;
    }

    public BigDecimal getMinX() {
        return minX.divide(this.zoom, 3, RoundingMode.HALF_UP).add(deslocamento.getX());
    }

    public void setMinX(BigDecimal minX) {
        this.minX = minX;
    }

    public BigDecimal getMaxY() {
        return maxY.divide(this.zoom, 3, RoundingMode.HALF_UP).add(deslocamento.getY());
    }

    public void setMaxY(BigDecimal maxY) {
        this.maxY = maxY;
    }

    public BigDecimal getMinY() {
        return minY.divide(this.zoom, 3, RoundingMode.HALF_UP).add(deslocamento.getY());
    }

    public void setMinY(BigDecimal minY) {
        this.minY = minY;
    }
    
    public List<Objeto2D> getObjetos() {
        return objetos;
    }

    public void setObjetos(List<Objeto2D> objetos) {
        this.objetos = objetos;
    }

    public BigDecimal getZoom() {
        return zoom.setScale(3, RoundingMode.CEILING);
    }

    public void setZoom(BigDecimal zoom) {
        if (zoom.doubleValue() > 0) {
            this.zoom = zoom;
        }
    }

    public Ponto getDeslocamento() {
        return deslocamento;
    }

    public void setDeslocamento(Ponto deslocamento) {
        this.deslocamento = deslocamento;
    }

    public Objeto2D getLinhaX() {
        return linhaX;
    }

    public Objeto2D getLinhaY() {
        return linhaY;
    }
    
}
