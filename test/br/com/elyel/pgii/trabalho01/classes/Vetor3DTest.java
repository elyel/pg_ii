package br.com.elyel.pgii.trabalho01.classes;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Elfab
 */
public class Vetor3DTest {
    
    private final Vetor vetor1;
    private final Vetor vetor2;
    private final Vetor vetor3;
    private final Vetor vetor4;
    
    public Vetor3DTest() {
        vetor1 = new Vetor(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE);
        vetor2 = new Vetor(12.0, 11.5, 12.6);
        vetor3 = new Vetor(10.0, 10.5, 0.00);
        vetor4 = new Vetor(2.00, 2.00, 0.00);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getX method, of class Vetor.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        
        assertEquals(BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP), vetor1.getX());
        assertEquals(new BigDecimal(12.0).setScale(4, RoundingMode.HALF_UP), vetor2.getX());
        assertEquals(new BigDecimal(10.0).setScale(4, RoundingMode.HALF_UP), vetor3.getX());
        assertEquals(new BigDecimal(2.0).setScale(4, RoundingMode.HALF_UP), vetor4.getX());
    }

    /**
     * Test of setX method, of class Vetor.
     */
    @Test
    public void testSetX() {
        System.out.println("setX");
        
        Vetor vetorTeste = new Vetor(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ZERO);
        vetorTeste.setX(new BigDecimal(111.2234567));
        
        
        assertEquals(new BigDecimal(111.2234567).setScale(4, RoundingMode.HALF_UP), vetorTeste.getX());
    }

    /**
     * Test of getY method, of class Vetor.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");

        assertEquals(BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP), vetor1.getX());
        assertEquals(new BigDecimal(12.0).setScale(4, RoundingMode.HALF_UP), vetor2.getX());
        assertEquals(new BigDecimal(10.0).setScale(4, RoundingMode.HALF_UP), vetor3.getX());
        assertEquals(new BigDecimal(2.0).setScale(4, RoundingMode.HALF_UP), vetor4.getX());
    }

    /**
     * Test of setY method, of class Vetor.
     */
    @Test
    public void testSetY() {
        System.out.println("setY");
        Vetor vetorTeste = new Vetor(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ZERO);
        vetorTeste.setX(new BigDecimal(111.2234567));
        
        
        assertEquals(new BigDecimal(111.2234567).setScale(4, RoundingMode.HALF_UP), vetorTeste.getX());
    }

    /**
     * Test of getZ method, of class Vetor.
     */
    @Test
    public void testGetZ() {
        System.out.println("getZ");
        
        assertEquals(BigDecimal.ONE.setScale(4, RoundingMode.HALF_UP), vetor1.getX());
        assertEquals(new BigDecimal(12.0).setScale(4, RoundingMode.HALF_UP), vetor2.getX());
        assertEquals(new BigDecimal(10.0).setScale(4, RoundingMode.HALF_UP), vetor3.getX());
        assertEquals(new BigDecimal(2.0).setScale(4, RoundingMode.HALF_UP), vetor4.getX());
    }

    /**
     * Test of setZ method, of class Vetor.
     */
    @Test
    public void testSetZ() {
        System.out.println("setZ");
        Vetor vetorTeste = new Vetor(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ZERO);
        vetorTeste.setX(new BigDecimal(111.2234567));
        
        
        assertEquals(new BigDecimal(111.2234567).setScale(4, RoundingMode.HALF_UP), vetorTeste.getX());
    }

    /**
     * Test of add method, of class Vetor.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        
        vetor1.add(vetor2);
        vetor3.add(vetor4);
        
        Vetor resultado1 = new Vetor(
                new BigDecimal(13.0).setScale(4, RoundingMode.HALF_UP), 
                new BigDecimal(12.5).setScale(4, RoundingMode.HALF_UP), 
                new BigDecimal(13.6).setScale(4, RoundingMode.HALF_UP)
        );
        
        Vetor resultado3 = new Vetor(
                new BigDecimal(12.0).setScale(4, RoundingMode.HALF_UP), 
                new BigDecimal(12.5).setScale(4, RoundingMode.HALF_UP), 
                new BigDecimal(0.0).setScale(4, RoundingMode.HALF_UP)
        );
        
        
        assertEquals(resultado1, vetor1);
        assertEquals(resultado3, vetor3);
        
    }

    /**
     * Test of addX method, of class Vetor.
     */
    @Test
    public void testAddX() {
        System.out.println("addX");
        BigDecimal x = new BigDecimal(2.2222);
        Vetor instance = new Vetor(0.1111, 9.9999, 1.0000);
        instance.addX(x);
        x = new BigDecimal(2.3333).setScale(4, RoundingMode.HALF_UP);
        
        assertEquals(x, instance.getX());
    }

    /**
     * Test of addY method, of class Vetor.
     */
    @Test
    public void testAddY() {
        System.out.println("addY");
        BigDecimal x = new BigDecimal(2.2222);
        Vetor instance = new Vetor(0.1111, 9.4444, 1.0000);
        instance.addY(x);
        x = new BigDecimal(11.6666).setScale(4, RoundingMode.HALF_UP);
        
        assertEquals(x, instance.getY());
    }

    /**
     * Test of addZ method, of class Vetor.
     */
    @Test
    public void testAddZ() {
        System.out.println("addZ");
        BigDecimal x = new BigDecimal(2.2222);
        Vetor instance = new Vetor(0.1111, 9.4444, 1.0000);
        instance.addZ(x);
        x = new BigDecimal(3.2222).setScale(4, RoundingMode.HALF_UP);
        
        assertEquals(x, instance.getZ());
    }

    /**
     * Test of sub method, of class Vetor.
     */
    @Test
    public void testSub() {
        System.out.println("sub");
        
        Vetor instance = new Vetor(0.1111, 9.4444, 1.0000);
        instance.sub(new Vetor(5.00, 6.00, 7.00));
        
        assertEquals(new Vetor(-4.8889, 3.4444, -6.0000), instance);
    }

    /**
     * Test of subX method, of class Vetor.
     */
    @Test
    public void testSubX() {
        System.out.println("subX");
        Vetor instance = new Vetor(0.5, 9.4444, 1.0000);
        instance.subX(BigDecimal.ONE);
        
        assertEquals(new Vetor(-0.5, 9.4444, 1.0000), instance);
    }

    /**
     * Test of subY method, of class Vetor.
     */
    @Test
    public void testSubY() {
        System.out.println("subY");
        Vetor instance = new Vetor(0.5, 9.4444, 1.0000);
        instance.subY(BigDecimal.ONE);
        
        assertEquals(new Vetor(0.5, 8.4444, 1.0000), instance);
    }

    /**
     * Test of subZ method, of class Vetor.
     */
    @Test
    public void testSubZ() {
        System.out.println("subZ");
        Vetor instance = new Vetor(0.5, 9.4444, 1.0000);
        instance.subZ(BigDecimal.ONE);
        
        assertEquals(new Vetor(0.5, 9.4444, 0.0000), instance);
    }

    /**
     * Test of mult method, of class Vetor.
     */
    @Test
    public void testMult() {
        System.out.println("mult");
        Vetor vetor3D = new Vetor(2.0, 2.0, 2.0);
        
        vetor3D.mult(2);
        assertEquals(new Vetor(4.0, 4.0, 4.0), vetor3D);
        
        vetor3D.mult(-2.0);
        assertEquals(new Vetor(-8.0, -8.0, -8.0), vetor3D);
        
        vetor3D.mult(-0.5);
        assertEquals(new Vetor(4.0, 4.0, 4.0), vetor3D);
        
    }
    
    @Test
    public void testMultMatriz(){
        ////////////////////////////////////////////////////////////////////////
        // TESTE 1 COM A MATRIZ IDENTIDADE
        ////////////////////////////////////////////////////////////////////////
        Matriz4x4 matriz4x4 = new Matriz4x4(
                new Vetor(1, 0, 0, 0), 
                new Vetor(0, 1, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
        
        Vetor vetorMultiplicador = new Vetor(10, 20, 30);
        Vetor vetorEsperado = new Vetor(10, 20, 30);
        Vetor vetorResultado = vetorMultiplicador.mult(matriz4x4);
        
        assertEquals(vetorEsperado, vetorResultado);
        
        ////////////////////////////////////////////////////////////////////////
        // TESTE 2 COM A MATRIZ DE ESCALONAMENTO
        ////////////////////////////////////////////////////////////////////////
        matriz4x4 = new Matriz4x4(
                new Vetor(2, 0, 0, 0), 
                new Vetor(0, 2, 0, 0), 
                new Vetor(0, 0, 2, 0), 
                new Vetor(0, 0, 0, 1)
        );
        
        vetorMultiplicador = new Vetor(10, 20, 30);
        vetorEsperado = new Vetor(20, 40, 60);
        vetorResultado = vetorMultiplicador.mult(matriz4x4);
        
        assertEquals(vetorEsperado, vetorResultado);
        
        ////////////////////////////////////////////////////////////////////////
        // TESTE 3 COM A MATRIZ DE TRANSLAÇÃO
        ////////////////////////////////////////////////////////////////////////
        matriz4x4 = new Matriz4x4(
                new Vetor(1, 0, 0, 0), 
                new Vetor(0, 1, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(10, 5, 1, 1)
        );
        
        vetorMultiplicador = new Vetor(10, 20, 30);
        vetorEsperado = new Vetor(20, 25, 31);
        vetorResultado = vetorMultiplicador.mult(matriz4x4);
        
        assertEquals(vetorEsperado, vetorResultado);
        
        ////////////////////////////////////////////////////////////////////////
        // TESTE 3 COM A MATRIZ DE ESCALONAMENTO + TRANSLACAO
        ////////////////////////////////////////////////////////////////////////
        
        matriz4x4 = new Matriz4x4(
                new Vetor(2, 0, 0, 0), 
                new Vetor(0, 2, 0, 0), 
                new Vetor(0, 0, 2, 0), 
                new Vetor(10, 5, 1, 1)
        );
        
        vetorMultiplicador = new Vetor(10, 20, 30);
        vetorEsperado = new Vetor(30, 45, 61);
        vetorResultado = vetorMultiplicador.mult(matriz4x4);
        
        assertEquals(vetorEsperado, vetorResultado);
        
        ////////////////////////////////////////////////////////////////////////
        // TESTE 3 COM A MATRIZ DE ROTAÇÃO HORARIO 90º
        ////////////////////////////////////////////////////////////////////////
        double cos90 = Math.cos( (Math.PI/180) * 90 );
        double sen90 = Math.sin( (Math.PI/180) * 90 );
        System.out.println("Cos(90) " + cos90);
        System.out.println("Sen(90) " + sen90);
        
        matriz4x4 = new Matriz4x4(
                new Vetor(cos90, (-sen90), 0, 0), 
                new Vetor(sen90, cos90, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
        
        vetorMultiplicador = new Vetor(-10, 0, 1);
        vetorEsperado = new Vetor(0, 10, 1);
        vetorResultado = vetorMultiplicador.mult(matriz4x4);
        
        assertEquals(vetorEsperado, vetorResultado);
        
        ////////////////////////////////////////////////////////////////////////
        // TESTE 3 COM A MATRIZ DE ROTAÇÃO ANTI-HORARIO 90º
        ////////////////////////////////////////////////////////////////////////
        matriz4x4 = new Matriz4x4(
                new Vetor(cos90, sen90, 0, 0), 
                new Vetor(-sen90, cos90, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
        
        vetorMultiplicador = new Vetor(0, 10, 1);
        vetorEsperado = new Vetor(-10, 0, 1);
        vetorResultado = vetorMultiplicador.mult(matriz4x4);
        
        assertEquals(vetorEsperado, vetorResultado);
    }

    /**
     * Test of div method, of class Vetor.
     */
    @Test
    public void testDiv() {
        System.out.println("div");
        Vetor vetor3D = new Vetor(2.0, 2.0, 2.0);
        
        vetor3D.div(2.0);
        assertEquals(new Vetor(1.0, 1.0, 1.0), vetor3D);
        
        vetor3D.div(-2.0);
        assertEquals(new Vetor(-0.5, -0.5, -0.5), vetor3D);
        
        vetor3D.div(-0.5);
        assertEquals(new Vetor(1.0, 1.0, 1.0), vetor3D);
    }

    /**
     * Test of mag method, of class Vetor.
     */
    @Test
    public void testGetMagnitude() {
        System.out.println("mag");
        Vetor vetor3D = new Vetor(2.0, 1.0, 1.0);
        
        
        assertEquals(new BigDecimal(2.449489743).setScale(4, RoundingMode.HALF_UP), vetor3D.getMagnitude());
    }
    /**
     * Test of normalizar method, of class Vetor.
     */
    @Test
    public void testNormalizar() {
        System.out.println("normalizar");
        
        Vetor vetor3D = new Vetor(2.0, 1.0, 1.0);
        Vetor resultado = new Vetor(0.8165, 0.4082, 0.4082);
        vetor3D.normalizar();
        assertEquals(resultado, vetor3D);
    }
    
    public void testAngulos(){
        System.out.println("Teste dos angulos");
        Vetor u = new Vetor(1.0, -2.0, 2.0);
        Vetor v = new Vetor(-3.0, 6.0, 6.0);
        
        assertEquals(new BigDecimal(180).setScale(4, RoundingMode.HALF_UP), u.getAngulo(v));
    }
    
}
