package br.com.elyel.pgii.trabalho01.classes;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Elfab
 */
public class Matriz4x4Test {
    
    public Matriz4x4Test() {
    }


    /**
     * Test of getMatrizIdentidade method, of class Matriz4x4.
     */
    @Test
    public void testGetMatrizIdentidade() {
        System.out.println("getMatrizIdentidade");
        Matriz4x4 expResult = new Matriz4x4(
                new Vetor(1, 0, 0, 0), 
                new Vetor(0, 1, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
        
        Matriz4x4 result = Matriz4x4.getMatrizIdentidade();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getCopia method, of class Matriz4x4.
     */
    @Test
    public void testGetCopia() {
        System.out.println("getCopia");
        Matriz4x4 matriz = new Matriz4x4(
                new Vetor(1, 0, 0, 0), 
                new Vetor(0, 1, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
        Matriz4x4 expResult = new Matriz4x4(
                new Vetor(1, 0, 0, 0), 
                new Vetor(0, 1, 0, 0), 
                new Vetor(0, 0, 1, 0), 
                new Vetor(0, 0, 0, 1)
        );
        
        Matriz4x4 result = Matriz4x4.getCopia(matriz);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getTransposta method, of class Matriz4x4.
     */
    @Test
    public void testGetTransposta() {
        System.out.println("getTransposta");
        Matriz4x4 matriz4x4 = new Matriz4x4(
                new Vetor(11, 12, 13, 14), 
                new Vetor(21, 22, 23, 24), 
                new Vetor(31, 32, 33, 34), 
                new Vetor(41, 42, 43, 44)
        );
        
        Matriz4x4 matriz4x4_transposta = new Matriz4x4(
                new Vetor(11, 21, 31, 41), 
                new Vetor(12, 22, 32, 42), 
                new Vetor(13, 23, 33, 43), 
                new Vetor(14, 24, 34, 44)
        );
        
        Matriz4x4 result = Matriz4x4.getTransposta(matriz4x4);
        assertEquals(matriz4x4_transposta, result);
        
        System.err.println("Matriz normal:\n" + matriz4x4);
        System.err.println("Matriz transposta:\n" + matriz4x4_transposta);
        System.err.println("Matriz transposta resultado 1:\n" + result);
        
        
        result = Matriz4x4.getTransposta(result);
        assertEquals(matriz4x4, result);
        System.err.println("Matriz transposta resultado 2:\n" + result);
        
    }
    
    @Test
    public void testMultiplicar() {
        System.out.println("multiplicar");
        Matriz4x4 A = new Matriz4x4(
                new Vetor(11, 12, 13, 0), 
                new Vetor(21, 22, 23, 0), 
                new Vetor(31, 32, 33, 0), 
                new Vetor(0, 0, 0, 0)
        );
        
        Matriz4x4 B = new Matriz4x4(
                new Vetor(1, 2, 3, 0), 
                new Vetor(4, 5, 6, 0), 
                new Vetor(7, 8, 9, 0), 
                new Vetor(0, 0, 0, 0)
        );
        
        Matriz4x4 AB = new Matriz4x4(
                new Vetor(150, 186, 222, 0), 
                new Vetor(270, 336, 402, 0), 
                new Vetor(390, 486, 582, 0), 
                new Vetor(0, 0, 0, 0)
        );
        
        String msg = "\nA: \n" + A.toString() + "\nB:\n" + B.toString() + "\nAB:\n" + A.multiplicar(B).toString();
        assertEquals(msg, AB, A.multiplicar(B));

    }
}
